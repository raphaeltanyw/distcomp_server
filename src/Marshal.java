/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jonathan
 */
public class Marshal {

    private static final String charSet = "UTF-8";  //Encoding to use in byte converation 

    /**
     * Return a Byte Array for the network module to process. As all class is
     * subclass of the Object superclass. The Object argument use the principle
     * of polymorphism to process the object.
     *
     * This method use reflection to access the class field and append the field
     * name and value to a string. This method also marshal any objects declared
     * in the object. GetByte() will be called to convert the string into a
     * byte[] format.
     *
     * @param object The object class to be marshal into byte[] array
     * @return serialByte Byte Array contain the content of marshaled object
     */
    public byte[] serialObject(Object object) {
        String serialString = "";
        byte[] serialByte = null;
        try {
            String className = object.getClass().getName();   //Obtain the name of declared object
            Class serialClass = Class.forName(className);    //Obtain the class object of the declared object
            serialString += className;
            Field[] fields = serialClass.getDeclaredFields(); //get the variable of the object included those declared as private
            for (Field field : fields) {
                field.setAccessible(true);                  //Turn off access check for field, for reflection only
                //serialString += "@";
                if (!field.getType().isPrimitive() && !field.getType().equals(String.class) && !field.getType().isInstance(object) && !field.getType().isArray()) { //check if field contain an user-created class
                    Object o = field.get(object);             //cast as an object
                    if (o.getClass().isEnum()) {                 //check if object is an emun
                        serialString += "@" + field.getName() + "#" +  o; //marshal as an normal field value
                    } else {
                        serialString += "<>" + new String(serialObject(o), charSet) + "<>";     //call this function again to serialize any object in the object               
                    }
                }else if(field.getType().isArray()) {
                    Object arrayO = field.get(object);
                    int length = Array.getLength(arrayO);
                    serialString += "@";
                    serialString += length + "#" + field.getName();
                    for(int i = 0; i < length; i++){
                        Object element = Array.get(arrayO, i);
                        
                        serialString += "?" + element.getClass().getName() + "#"  +element;
                    }
                }else {
                    serialString += "@" + field.getName() + "#" + field.get(object);  //else get the name and value of the field instance                  
                }
                serialByte = serialString.getBytes(charSet);            //convert the string into an byte[] array with a specific character encoding
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        }

        return serialByte;
    }
    
     /**
     * Return a Object for module to processing. As all class is subclass of the
     * Object superclass. The return argument use the principle of polymorphism
     * to return the object for further processing.
     *
     * This method received the byte array from the network module and convert
     * it to an string. The string is split by a delimiter to get the individual
     * variable and value. This substring is added to an ArrayList and is used
     * as a parameter to a function called to initialize an object.
     *
     * @param data Byte array received from the network module
     * @return Object for further processing
     */
    public Object unSerialObject(byte[] data) {
        Object o = null;
        try {
            String serialString = new String(data, charSet);    //convert the byte array to string        
            String objectString = "";
            ArrayList<String> parameterList = new ArrayList<>();
            if (serialString.contains("<>")) { // check if there is an inner object class
                objectString = serialString.substring(serialString.indexOf("<>") + 2, serialString.lastIndexOf("<>")); //set the inner object class string
                serialString = serialString.replace(objectString, ""); // remove the inner object class from outer object class
                serialString = serialString.replace("<>", "");        //remove any remaining object tag                         
            }
            String[] st = serialString.split("@"); //delimit the string to get the individual variable and value
            for (String s : st) {
                //add the token of the string array to the parameterList arrayList
                parameterList.add(s.trim());             
            }
            if (objectString.length() != 0) {
                parameterList.add(objectString);
            }
            //call function to initialize and set object
            o = initializeObject(parameterList);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }

    //use reflection to create and initialize Object
    private Object initializeObject(ArrayList<String> parameterList) {
        Object o = null;
        try {
            //get the class name of the object to initialize
            String temp = parameterList.remove(0);
            Class oClass = Class.forName(temp); //obtain the class object
            Constructor contructor = oClass.getConstructor(); //get the constructor of the class object
            o = contructor.newInstance();   //create a new object
            Method[] methods = oClass.getMethods();          //get the method list of the class object   
            Field[] fields = oClass.getDeclaredFields();    //get the fields list of the class object

            for (Method m : methods) { //loop through the avaible method
                for (String s : parameterList) { //loop the parameterlist
                    if (s.contains("@")) { //check if parameter is an inner object class                       
                        Object oo = unSerialObject(s.getBytes(charSet)); //called the function to parse the serialString and initialize the object
                        if (isSetter(m)) { //check if the method is a set method
                            if (m.getName().toLowerCase().contains(oo.getClass().getSimpleName())) { //check if the variable name match the method to called
                                m.invoke(o, oo); //invoke the method with the object as a parameter
                                parameterList.remove(s);
                                break;
                            }
                        }
                    } else if (s.contains("?")) {   //check if paramater is an array                       
                        if (isSetter(m)) {
                            String[] innerString = s.split("\\?"); //split the array to get the array name and length of array
                            String[] iArrayParamter = innerString[0].split("#"); //split the name and length
                            if (m.getName().toLowerCase().contains(iArrayParamter[1])) { //check if the method is the one to call
                                Object[] oArray = (Object[]) Array.newInstance(Object.class, Integer.parseInt(iArrayParamter[0])); //instalize array with provided length
                                for (int i = 1; i < innerString.length; i++) {                                    
                                    String[] eValue = innerString[i].split("#"); //split the inner value along with class type
                                    Object ov = valueOf(Class.forName(eValue[0]), eValue[1]); //class the valueof in the class to get cast string to the object type
                                    Array.set(oArray, (i - 1), ov); //set the value at the specific location
                                }
                                m.invoke(o, oArray);
                                parameterList.remove(s);
                            }
                        }
                    } else {
                        String[] innerString = s.split("#");     //delimiter string into two token, one variable name , variable name                   
                        if (isSetter(m)) {
                            if (m.getName().toLowerCase().contains(innerString[0])) {
                                if (m.getParameterTypes().length == 1) {
                                  
                                    //some of the method can not case from string so need to intalize as primative variable
                                    if (m.getParameterTypes()[0].getName().equalsIgnoreCase("float")) {
                                        float tempValue = Float.parseFloat(innerString[1]);
                                        m.invoke(o, tempValue);
                                        parameterList.remove(s);
                                        break;
                                    } else if (m.getParameterTypes()[0].getName().equalsIgnoreCase("int")) {
                                        int tempValue = Integer.parseInt(innerString[1]);
                                        m.invoke(o, tempValue);
                                        parameterList.remove(s);
                                        break;
                                    } else if (m.getParameterTypes()[0].getName().equalsIgnoreCase("long")) {
                                        long tempValue = Long.parseLong(innerString[1]);
                                        m.invoke(o, tempValue);
                                        parameterList.remove(s);
                                        break;
                                    } else {
                                        m.invoke(o, innerString[1]);
                                        parameterList.remove(s);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //check if the parameter list is not empty due to some varibale without set method
            if (parameterList.size() > 0) {
                for (String s : parameterList) {
                    
                    //set the variable by field
                    for (Field f : fields) {
                        if (s.contains("?")) {  //for arrayList
                            //same operation as the one described above
                            String[] innerString = s.split("\\?");
                            String[] iArrayParamter = innerString[0].split("#");
                            if (f.getName().toLowerCase().contains(iArrayParamter[1])) {
                                f.setAccessible(true);
                                Object[] oArray = (Object[]) Array.newInstance(Object.class, Integer.parseInt(iArrayParamter[0])); //instalize array with provided length
                                for (int i = 1; i < innerString.length; i++) {                                    
                                    String[] eValue = innerString[i].split("#");
                                    Object ov = valueOf(Class.forName(eValue[0]), eValue[1]);                                    
                                    Array.set(oArray, (i - 1), ov);                                         
                                }                    
                                f.set(o, oArray);
                                parameterList.remove(s);
                                
                                break;
                            }
                        } else {
                            String[] innerString = s.split("#");
                            if (f.getName().toLowerCase().contains(innerString[0])) {
                                f.setAccessible(true);
                                f.set(o, innerString[1]);
                                parameterList.remove(s);
                                break;
                            }
                        }
                    }                    
                }
                
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Marshal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }

    //casting of value to orginal value
    static <T> T valueOf(Class<T> klazz, String arg) {
        Exception cause = null;
        T ret = null;
        try {
            ret = klazz.cast(
                    klazz.getDeclaredMethod("valueOf", String.class)
                    .invoke(null, arg)
            );
        } catch (NoSuchMethodException e) {
            cause = e;
        } catch (IllegalAccessException e) {
            cause = e;
        } catch (InvocationTargetException e) {
            cause = e;
        }
        if (cause == null) {
            return ret;
        } else {
            throw new IllegalArgumentException(cause);
        }
    }

    //check method is a set method
    private static boolean isSetter(Method method) {
        if (!method.getName().startsWith("set")) {
            return false;
        }
        if (method.getParameterTypes().length != 1) {
            return false;
        }
        return true;
    }
}
