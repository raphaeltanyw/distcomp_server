
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jonathan Poh
 */
public class UDPServerSendThread implements Runnable {

    private LinkedBlockingQueue<MessageObject> outQueue;
    DatagramSocket aSocket;
    byte[] buffer;
    private Marshal marshaller = new Marshal();
    volatile  boolean switchTh = true;

    public UDPServerSendThread(LinkedBlockingQueue<MessageObject> outQueue, DatagramSocket aSocket) {
        this.outQueue = outQueue;
        this.aSocket = aSocket;
    }

    @Override
    public void run() {
        while (switchTh) {
            try {
                MessageObject mo = outQueue.poll();
                if(mo!= null){
	                buffer = marshaller.serialObject(mo);
	                DatagramPacket reply = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(mo.getIpAddress()), mo.getPort());
	                aSocket.send(reply);
                }
            } catch (UnknownHostException ex) {
                Logger.getLogger(UDPServerSendThread.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(UDPServerSendThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void terminate() {
        this.switchTh = false;
    }

}
