
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jonathan Poh
 */
public class UDPServerReceiveThread implements Runnable {
    
    
    private LinkedBlockingQueue<MessageObject> inQueue;
    DatagramSocket aSocket;
    byte[] buffer = new byte[1000];
    private Marshal marshaller = new Marshal();
    volatile boolean switchTh = true;
    
    public UDPServerReceiveThread(LinkedBlockingQueue<MessageObject> inQueue, DatagramSocket aSocket ){
        this.inQueue = inQueue;
        this.aSocket = aSocket;
    }
    
    
    
    @Override
    public void run() {
        while(switchTh){
            try {
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                aSocket.receive(request);
                MessageObject mo = (MessageObject) marshaller.unSerialObject(request.getData());
                inQueue.add(mo);
            } catch (IOException ex) {
                Logger.getLogger(UDPServerReceiveThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
   public void terminate() {
        this.switchTh = false;
    }
    
}
