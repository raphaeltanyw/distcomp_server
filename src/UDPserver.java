import java.net.*;
import java.io.*;

/**
 *
 * @author Adrian
 */
public class UDPserver {
    
    public static void main(String args[]){
        DatagramSocket aSocket = null;
        try{
            aSocket = new DatagramSocket(6789);
            //bound to host and port
            byte[] buffer = new byte[1000];
            while(true){
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                aSocket.receive(request); //blocked if no input
                DatagramPacket reply = new DatagramPacket(request.getData(), request.getLength(),request.getAddress(), request.getPort());
                //to reply, send back to the same port
                aSocket.send(reply);
            }
        } //handle exceptions
            catch (SocketException e) {
            System.out.println("Socket: "+e.getMessage());
        } catch (IOException e) {
            System.out.println("IO: "+e.getMessage());
        }
           finally {
                if (aSocket != null) {
                    aSocket.close();
                }
            }
    } //close main method
        
} //close UDPServer
