
public class BankAccount {
	
	public enum CurrencyEnum {
		SGD, USD, MYR, AUD, GBP, EUR, IDR, JPY, HKD, TWD, CNY;
		
		public static CurrencyEnum getValue(String str){
			if(USD.name().equalsIgnoreCase(str)){
				return USD;
			}else if(SGD.name().equalsIgnoreCase(str)){
				return SGD;
			}else if(MYR.name().equalsIgnoreCase(str)){
				return MYR;
			}else if(AUD.name().equalsIgnoreCase(str)){
				return AUD;
			}else if(GBP.name().equalsIgnoreCase(str)){
				return GBP;
			}else if(EUR.name().equalsIgnoreCase(str)){
				return EUR;
			}else if(IDR.name().equalsIgnoreCase(str)){
				return IDR;
			}else if(JPY.name().equalsIgnoreCase(str)){
				return JPY;
			}else if(HKD.name().equalsIgnoreCase(str)){
				return HKD;
			}else if(TWD.name().equalsIgnoreCase(str)){
				return TWD;
			}else if(CNY.name().equalsIgnoreCase(str)){
				return CNY;
			}
			return SGD;
		}
	};

	private int accountNum;
	private String name;
	private String password;
	private float balance;
	private CurrencyEnum currency;
	
	public BankAccount(int accountNum, String name, String password, CurrencyEnum curr, float balance){
		this.accountNum = accountNum;
		this.name = name;
		this.password = password;
		this.currency = curr;
		this.balance = balance;
		
		//account number??
	}
	
	public CurrencyEnum getCurrency(){
		return this.currency;
	}
	
	public boolean setCurrency(CurrencyEnum curr){
		this.currency = curr;
		return true;
	}
	
	public float getBalance(){
		return this.balance;
	}
	
	public boolean setBalance(float bal){
		this.balance = bal;
		return true;
	}
	
	public int getAccountNum(){
		return accountNum;
	}
	
	public boolean checkPassword(String enteredPass){
		if(password==enteredPass){
			return true;
		}
		return false;
	}
	
	public String getName(){
		return name;
	}
}
