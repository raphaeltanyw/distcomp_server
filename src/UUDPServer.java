
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jonathan Poh
 */
public class UUDPServer implements Runnable{

    //send to client queue
    private LinkedBlockingQueue<MessageObject> receiveQueue; //to be send to client
    private LinkedBlockingQueue<MessageObject> sendQueue;    //received from client
   
    //send to and from server for processing        
    private LinkedBlockingQueue<MessageObject> inQueue; //to recieved by the upper layer
    private LinkedBlockingQueue<MessageObject> outQueue; //to send by UDP server to client
           
       
    //for history of sending out message
    private Map<String, MessageObject> tranHistory;
    
    //keep track of message history when come in
    private ArrayList<String> messageHistory;
    
    //thread object 
    private UDPServerReceiveThread udpServerReceiveThread;
    private UDPServerSendThread udpServerSendThread;
    
    //create thread
    Thread receivedThread;
    Thread sendThread;
    
    DatagramSocket sendSocket = null;
    DatagramSocket receivedSocket =null;
    
    private final int serverPort = 2222;
    private final int outServerPort = 3333;
    volatile boolean switchTh = true;
           
    private UUDPServer(LinkedBlockingQueue<MessageObject> inQueue, LinkedBlockingQueue<MessageObject> outQueue ) {        
        
        try {
            //intialize queue for send and received from client
            sendQueue = new LinkedBlockingQueue<>();
            receiveQueue = new LinkedBlockingQueue<>();
            
            this.inQueue = inQueue;
            this.outQueue = outQueue;
            //create network socket
            sendSocket = new DatagramSocket(outServerPort);
            receivedSocket = new DatagramSocket(serverPort);
            //intialize history object
            tranHistory = new HashMap<>();
            messageHistory = new ArrayList<>();
            
            //create recieving and sending thread object
            udpServerReceiveThread = new UDPServerReceiveThread(receiveQueue, receivedSocket);
            udpServerSendThread = new UDPServerSendThread(sendQueue, sendSocket);
            
            //create thread and start it
            receivedThread = new Thread(udpServerReceiveThread);
            sendThread = new Thread(udpServerSendThread);             
        } catch (SocketException ex) {
           Logger.getLogger(UUDPServer.class.getName()).log(Level.SEVERE, null, ex);        
        }
    }
    
    //prevent thread start in constructor error
    public static UUDPServer getUDPServer(LinkedBlockingQueue<MessageObject> inQueue, LinkedBlockingQueue<MessageObject> outQueue){ 
        UUDPServer udpServer = new UUDPServer(inQueue, outQueue);
        udpServer.startSendReceiveThread();
        return udpServer;
        
    }
    
    //Start in internal thread for send and receive  
    private void startSendReceiveThread(){
        receivedThread.start();
        sendThread.start();
    }

    @Override
    public void run() {
        while(switchTh){          
            //retrieve from queue to be send to client
            MessageObject outClMsg = outQueue.poll();
            if(outClMsg != null){
                //at least once invocation method
                if(outClMsg.getNetworkType() == 0){
                    sendQueue.add(outClMsg);
                }else{
                    //at most once method
                    String temp = outClMsg.getIpAddress() +";"+ String.valueOf(outClMsg.getRequestID()); //key contain ipaddress and requestid
                    tranHistory.put(temp, outClMsg);
                    sendQueue.add(outClMsg);
                }
            }
            
            //retrive from queue for object that retrieve from client
            MessageObject inClMsg = receiveQueue.poll();
            if(inClMsg != null){
                //at least once Invocation method
                if(inClMsg.getNetworkType() == 0){
                    inQueue.add(inClMsg);
                }else{
                    //at most once method
                    String temp = inClMsg.getIpAddress() +";"+ String.valueOf(inClMsg.getRequestID());
                    //check if tran is in history
                    if(messageHistory.contains(temp)){
                        sendQueue.add(tranHistory.get(temp));
                    }else{
                        //new message
                        messageHistory.add(temp); //add to message History
                        inQueue.add(inClMsg);
                    }
                }
            }
           
        }
        
    }
    
    public void terminate() {
        try {
            udpServerReceiveThread.terminate();;
            receivedThread.join();
            
            udpServerSendThread.terminate();
            sendThread.join();
            this.switchTh = false;
        } catch (InterruptedException ex) {
            Logger.getLogger(UUDPServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
