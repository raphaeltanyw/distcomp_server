
public class MessageObject {

	private String ipAddress;
	private int portNum;
	private int typeReqRep;
	private int requestID;
	private int operationType;
	Object[] operationParameters;
	private int networkType;
	
	public MessageObject(String ipAddress, int portNum, int typeReqRep, int requestID, int operationType, Object[] operationParameters){
		this.ipAddress = ipAddress;
		this.portNum = portNum;
		this.typeReqRep = typeReqRep;
		this.requestID = requestID;
		this.operationType = operationType;
		this.operationParameters = operationParameters;
	}
	
	/*
	 * return IP address of message origin
	 */
	public String getIpAddress(){
		return ipAddress;
	}
	
	/*
	 * return port of message origin
	 */
	public int getPort(){
		return portNum;
	}
	
	/*return type (request or reply)
	0 = request, 1 = reply
	*/
	public int getTypeReqRep(){
		return typeReqRep;
	}

	/*
	 * return request ID assigned by client
	 */
	public int getRequestID(){
		return requestID;
	}
	
	/*
	 * return the type of operation to be done.
	 1 = create account
	 2 = close account
	 3 = withdraw from account
	 4 = deposit to account
	 5 = check balance
	 6 = ???
	 7 = callback registration
	 */
	public int getOperationType(){
		return operationType;
	}
	
	/*
	 * get parameters needed for the operation (e.g. account number, name, password, etc)
	 */
	public Object[] getOperationParameters(){
		return operationParameters;
	} 
	
	
	/*
	 * Get network type for invocation method
	 * 0 = at least once
	 * 1 =at most once
	 */
	 public int getNetworkType(){
		 return networkType;
	 }
}
